# KochExplorer

Freely control a recursive shape to draw and explore your custom fractal.

Inspired by [Koch snowflake](https://en.wikipedia.org/wiki/Koch_snowflake).

Gamepad is handled, but only with XBox button layout in mind.

![Screenshot](screenshot/base.png)
![Screenshot](screenshot/banzai.png)
![Screenshot](screenshot/cactus.png)

## Build dependencies

* cmake
* C++17 compiler
* SFML

Procedure for Debian/Ubuntu:

```
sudo apt install cmake g++ libsfml-dev
make
```

## Runtime dependencies

* SFML

Procedure for Debian/Ubuntu:

```
sudo apt install libsfml-graphics2.5
```

## TODO

* Compilation for windows with MinGW
* Proper management of system ressources (save path,font)
* Zoom factor on window resize may be simplified
* json loader
* undo
* interpolation

