#include "KochExplorer.hpp"
#include <iostream>
#include <sstream>
#include <fstream>
#include <chrono>
#include <filesystem>

#include "XboxGP.hpp"
// some utils
namespace {

std::string nowString()
{
	std::stringstream result;

	auto const now = std::chrono::system_clock::now();
	time_t tt = std::chrono::system_clock::to_time_t( now );
	struct tm * lt = localtime(&tt);

	result << (1900+lt->tm_year);
	result << std::setw(2) << std::setfill('0') << lt->tm_mon;
	result << std::setw(2) << std::setfill('0') << lt->tm_mday;
	result << std::setw(2) << std::setfill('0') << lt->tm_hour;

	result << std::setw(2) << std::setfill('0') << lt->tm_min;
	result << std::setw(2) << std::setfill('0') << lt->tm_sec;
	return result.str();
}


}

KochExplorer::KochExplorer(ssa::ProgramOptions const & po)
	: ssa::Application<AppConfig>(po)
{
	recRule.recursiveLine = Line::kochLine;
	baseShape = Line::triangleShape;

	shapeCtrl.setCurrentLine( recRule.recursiveLine );
	shapeCtrl.setCurrentVertex(2);

	// Window icon
	{
		sf::RenderTexture tex;
		tex.create(64,64);
		tex.clear(getConfig().colorBackground);

		sf::Transform zoom;
		zoom.translate(tex.getSize().x/2.,tex.getSize().y/3.);
		zoom.scale(40,40);
		zoom.translate(-0.5,-0.);
		recursiveRenderer.computeShape(tex, recRule, baseShape, zoom);
		recursiveRenderer.draw(tex);

		sf::Image icon = tex.getTexture().copyToImage();
		window.setIcon(icon.getSize().x, icon.getSize().y, icon.getPixelsPtr());
	}
}




void KochExplorer::writeParams(std::string const & filename)
{
	using namespace std;
	std::ofstream f(filename);
	f << "{" << endl;

	Line & line = recRule.recursiveLine;

	f << "	\"line\": [" << endl;
	for ( size_t i=0 ; i<line.size() ; ++i )
	{
		f << "		[ " << line[i].position.x << " , " << line[i].position.y << " ]";
		if (i == (line.size()-1))
		{
			f << " ]," << endl;
		}
		else
		{
			f << ',' << endl;
		}
	}
	f << "	\"legType\": [" << endl;
	for ( size_t i=1 ; i<line.size() ; ++i )
	{
		f << "		\"" << Line::getLegType_str(line.getLegType(i-1)) << "\"";
		if (i == (line.size()-1))
		{
			f << " ]," << endl;
		}
		else
		{
			f << ',' << endl;
		}
	}
	f << "	\"alternateX\": " << std::boolalpha << recRule.shouldAlternateX << endl;
	f << "	\"alternateY\": " << std::boolalpha << recRule.shouldAlternateY << endl;

	f << "	\"view\": [ " << recursiveRenderer.viewShift.x << " , " << recursiveRenderer.viewShift.y << " ]," << endl;
	f << "	\"zoom\": " << recursiveRenderer.zoomFactor << endl;
	f << "}" << endl;
}

void KochExplorer::save()
{
	try
	{
		std::filesystem::create_directory("screenshot/");
	}
	catch (std::exception const & e)
	{
		std::cout << "e.what()" << std::endl;
	}

	sf::Vector2u windowSize = window.getSize();
	sf::Texture texture;
	texture.create(windowSize.x, windowSize.y);
	texture.update(window);
	sf::Image screenshot = texture.copyToImage();

	std::string filename = "screenshot/" + nowString();
	screenshot.saveToFile(filename+".png");
	writeParams(filename+".json");
	status.notify(filename);
}


void KochExplorer::onWindowResize()
{
	miniMapZoom = sf::Transform();
	miniMapZoom.translate( window.getSize().x-150, window.getSize().y-100. );
	miniMapZoom.scale(100.f,100.f);
}



void KochExplorer::drawWindow()
{
	mouseDrag.rules.clear();

	recursiveRenderer.draw(window);

	if (shouldDrawMinimap)
	{
		// mini map
		drawMinimap(window, shapeCtrl.getCurrentLine(), frameTime, miniMapZoom);

		// register hovering zones
		for (size_t i=0; i<shapeCtrl.getCurrentLine().size(); i++)
		{
			sf::Vector2f const & p = shapeCtrl.getCurrentLine()[i].position;
			ssa::MouseDrag::HoveringZone rule;
			rule.target = i;
			rule.point = window.mapCoordsToPixel(miniMapZoom.transformPoint(p));
			mouseDrag.rules.push_back(rule);
		}
	}

}

void KochExplorer::drawMinimap(sf::RenderWindow & window, Line const & line, sf::Time time, sf::Transform const & initialTransform)
{
	// Draw segment
	if (line.size()>1)
	{
		window.draw(line.data(), line.size(), sf::LineStrip, initialTransform);

		size_t nbSegments = line.size()-1;
		for (size_t i=0; i<nbSegments; ++i)
		{
			auto type = line.getLegType(i);
			if (type != LegType::Terminal)
			{
				sf::Vertex s[3] = {
					sf::Vector2f(1/3.f, -1/16.f),
					sf::Vector2f(2/3.f, -1/16.f),
					sf::Vector2f(2/3.f, -1/16.f) - sf::Vector2f(1/16.f, 1/32.f),
				};
				auto t = initialTransform*getSegmentTransform(line,i);

				window.draw(s, 3, sf::LineStrip, t);
			}
		}
	}

	if (&shapeCtrl.getCurrentVextex() > line.data() and not line.empty())
	{
		sf::Vertex s[2] = { shapeCtrl.getCurrentVextex(), *(&shapeCtrl.getCurrentVextex()-1) };
		s[0].color = getConfig().colorHighlight;
		s[1].color = getConfig().colorHighlight;
		window.draw(s, 2, sf::LineStrip, initialTransform);
	};


	// Draw vertices
	sf::CircleShape dot(0.02f, 4);
	dot.setOrigin(dot.getRadius(),dot.getRadius());
	for (auto const & p : line)
	{
		dot.setPosition(p.position);

		if (&p == &shapeCtrl.getCurrentVextex())
		{
			float const breathFactor = std::abs( (static_cast<int>(time.asMilliseconds())%512)/256.f - 1.f );
			float const scale = 1.f + breathFactor;
			dot.setFillColor(getConfig().colorHighlight);
			dot.setScale(scale,scale);
		}
		else
		{
			dot.setFillColor(getConfig().colorForeground);
			dot.setScale(1.f,1.f);
		}

		window.draw(dot, initialTransform);
	}
}

float KochExplorer::getSpeedFactor() const
{
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::LShift) ||
		sf::Keyboard::isKeyPressed(sf::Keyboard::RShift))
	{
		return 8.;
	}
	else if (sf::Keyboard::isKeyPressed(sf::Keyboard::LAlt))
	{
		return 1./8.;
	}
	return 1.;
}

void KochExplorer::updateState() {

	recursiveRenderer.update(frameDuration);

	{
		// main drawing

		sf::Transform mainZoom;
		mainZoom.translate(window.getSize().x/2.,window.getSize().y/2.);
		mainZoom.scale(recursiveRenderer.zoomFactor,recursiveRenderer.zoomFactor);
		mainZoom.translate(recursiveRenderer.viewShift);
		mainZoom.translate(-0.5,0);

		if (recursiveRenderer.shouldDrawFullShape)
		{
			recursiveRenderer.computeShape(window, recRule, baseShape, mainZoom);
		}
		else
		{
			recursiveRenderer.compute(window, recRule, mainZoom);
		}

	}
	debugInfo << std::setprecision(3) <<
		"Alternate: " << (recRule.shouldAlternateX ? 'X':' ') << (recRule.shouldAlternateY ? 'Y':' ') << "\n"
		"Zoom: x"    << (recursiveRenderer.zoomFactor/RecursiveRenderer::defaultZoomFactor) << "\n"
		"Threshold: "<< recursiveRenderer.detailThreshold << " px - Max Depth: " << recursiveRenderer.getMaxNbOfRecursions() << (recursiveRenderer.forceLowDef?" (Low Def)":"") << "\n"
		"Segments: " << recursiveRenderer.getSegmentCount() << " - " << recursiveRenderer.segmentTotalLength << " px \n"
		"Iterations: " << recursiveRenderer.nbIteration << "\n";

}

void KochExplorer::handleEvent(sf::Event const & event)
{
	if (event.type == sf::Event::KeyPressed)
	{
		if (event.key.code == sf::Keyboard::Home)
		{
			recursiveRenderer.resetView();
		}
		else if (event.key.code == sf::Keyboard::S && event.key.control)
		{
			save();
		}
		else if (event.key.code == sf::Keyboard::Tab)
		{
			if (event.key.shift)
			{
				shapeCtrl.selectBack();
			}
			else
			{
				shapeCtrl.selectNext();
			}
		}
		else if (event.key.code == sf::Keyboard::M)
		{
			shouldDrawMinimap = !shouldDrawMinimap;
		}
		else if (event.key.code == sf::Keyboard::W)
		{
			recursiveRenderer.switchHighLowDef();
			status.notify( recursiveRenderer.forceLowDef ? "Wireframe" : "HD" );
		}
		else if (event.key.code == sf::Keyboard::PageUp)
		{
			if (event.key.shift)
			{
				recursiveRenderer.increaseDetailThreshold();
			}
			else
			{
				recursiveRenderer.recLevelDecrease();
				status.notify(std::to_string(recursiveRenderer.getMaxNbOfRecursions()));
			}
		}
		else if (event.key.code == sf::Keyboard::PageDown)
		{
			if (event.key.shift)
			{
				recursiveRenderer.decreaseDetailThreshold();
			}
			else
			{
				recursiveRenderer.recLevelIncrease();
				status.notify(std::to_string(recursiveRenderer.getMaxNbOfRecursions()));
			}
		}
		else if (event.key.code == sf::Keyboard::V)
		{
			recursiveRenderer.shouldDrawFullShape = !recursiveRenderer.shouldDrawFullShape;
		}
		else if (event.key.code == sf::Keyboard::X)
		{
			if (event.key.shift)
				recRule.shouldAlternateX = !recRule.shouldAlternateX;
			else
				shapeCtrl.toggleInvertX();
		}
		else if (event.key.code == sf::Keyboard::Y)
		{
			if (event.key.shift)
				recRule.shouldAlternateY = !recRule.shouldAlternateY;
			else
				shapeCtrl.toggleInvertY();
		}
		else if (event.key.code == sf::Keyboard::A)
		{
			shapeCtrl.append();
		}
		else if (event.key.code == sf::Keyboard::D)
		{
			shapeCtrl.divideCurrentLeg();
		}
		else if (event.key.code == sf::Keyboard::Delete)
		{
			shapeCtrl.deleteCurrent();
			shapeCtrl.selectNext();
		}
		else if (event.key.code == sf::Keyboard::Backspace)
		{
			shapeCtrl.deleteCurrent();
		}
		else if (event.key.code == sf::Keyboard::L)
		{
			if (&shapeCtrl.getCurrentLine() == &baseShape)
			{
				shapeCtrl.setCurrentLine(recRule.recursiveLine);
			}
			else
			{
				shapeCtrl.setCurrentLine(baseShape);
			}
		}
		else if (event.key.code == sf::Keyboard::T)
		{
			shapeCtrl.toggleTerminal();
		}
	}
	else if(event.type == sf::Event::MouseWheelMoved)
	{
		float const delta = event.mouseWheel.delta * getSpeedFactor() / 16.;
		recursiveRenderer.zoomIn(delta);
	}

	else if (event.type == sf::Event::JoystickButtonPressed)
	{
		if (event.joystickButton.button == XboxGP::Btn_A)
		{
			shapeCtrl.divideCurrentLeg();
		}
		else if (event.joystickButton.button == XboxGP::Btn_B)
		{
			shapeCtrl.deleteCurrent();
		}
		else if (event.joystickButton.button == XboxGP::Btn_Y)
		{
			shapeCtrl.toggleInvertY();
		}
		else if (event.joystickButton.button == XboxGP::Btn_X)
		{
			shapeCtrl.toggleInvertX();
		}
		else if (event.joystickButton.button == XboxGP::Btn_R)
		{
			shapeCtrl.toggleTerminal();
		}
		else if (event.joystickButton.button == XboxGP::Btn_Left)
		{
			shapeCtrl.selectBack();
		}
		else if (event.joystickButton.button == XboxGP::Btn_Right)
		{
			shapeCtrl.selectNext();
		}
		else if (event.joystickButton.button == XboxGP::Btn_Up)
		{
			shapeCtrl.setCurrentLine(recRule.recursiveLine);
		}
		else if (event.joystickButton.button == XboxGP::Btn_Down)
		{
			shapeCtrl.setCurrentLine(baseShape);
		}
	}
}

void KochExplorer::handleSSAEvent(ssa::Event const & event)
{
	if (event.eventType == ssa::EventType::Hovered)
	{
		if (event.target != ssa::MouseDrag::WINDOW)
		{
			shapeCtrl.setCurrentVertex(event.target);
		}
	}
	else if (event.eventType == ssa::EventType::Dragged)
	{
		if (event.target == ssa::MouseDrag::WINDOW)
		{
			recursiveRenderer.moveView(-window.mapPixelToCoords(event.shift));
		}
		else
		{
			shapeCtrl.getCurrentVextex().position += window.mapPixelToCoords(event.shift)/miniMapZoom.getMatrix()[0];
		}
	}
}

void KochExplorer::handleKeyPressed()
{
	float const step = frameDuration.asSeconds() * getSpeedFactor() / 16.;

	if (sf::Keyboard::isKeyPressed(sf::Keyboard::Up))
	{
		shapeCtrl.getCurrentVextex().position.y -= step;
	}
	else if (sf::Keyboard::isKeyPressed(sf::Keyboard::Down))
	{
		shapeCtrl.getCurrentVextex().position.y += step;
	}

	if (sf::Keyboard::isKeyPressed(sf::Keyboard::Left))
	{
		shapeCtrl.getCurrentVextex().position.x -= step;
	}
	else if (sf::Keyboard::isKeyPressed(sf::Keyboard::Right))
	{
		shapeCtrl.getCurrentVextex().position.x += step;
	}

	if (sf::Keyboard::isKeyPressed(sf::Keyboard::Num1))      { shapeCtrl.setCurrentVertex(0); }
	else if (sf::Keyboard::isKeyPressed(sf::Keyboard::Num2)) { shapeCtrl.setCurrentVertex(1); }
	else if (sf::Keyboard::isKeyPressed(sf::Keyboard::Num3)) { shapeCtrl.setCurrentVertex(2); }
	else if (sf::Keyboard::isKeyPressed(sf::Keyboard::Num4)) { shapeCtrl.setCurrentVertex(3); }
	else if (sf::Keyboard::isKeyPressed(sf::Keyboard::Num5)) { shapeCtrl.setCurrentVertex(4); }
	else if (sf::Keyboard::isKeyPressed(sf::Keyboard::Num6)) { shapeCtrl.setCurrentVertex(5); }
	else if (sf::Keyboard::isKeyPressed(sf::Keyboard::Num7)) { shapeCtrl.setCurrentVertex(6); }
	else if (sf::Keyboard::isKeyPressed(sf::Keyboard::Num8)) { shapeCtrl.setCurrentVertex(7); }
	else if (sf::Keyboard::isKeyPressed(sf::Keyboard::Num9)) { shapeCtrl.setCurrentVertex(8); }
	else if (sf::Keyboard::isKeyPressed(sf::Keyboard::Num0)) { shapeCtrl.setCurrentVertex(9); }

	if (sf::Joystick::isConnected(0))
	{
		{
			float const x = XboxGP::joystickCurved( sf::Joystick::getAxisPosition(0, XboxGP::Joystick_LX) );
			float const y = XboxGP::joystickCurved( sf::Joystick::getAxisPosition(0, XboxGP::Joystick_LY) );
			recursiveRenderer.moveView( (step * 256.f) * sf::Vector2f(x,y) );
		}

		{
			float const x = XboxGP::joystickCurved( sf::Joystick::getAxisPosition(0, XboxGP::Joystick_RX) );
			float const y = XboxGP::joystickCurved( sf::Joystick::getAxisPosition(0, XboxGP::Joystick_RY) );
			shapeCtrl.getCurrentVextex().position += (step * 64.f / recursiveRenderer.zoomFactor) * sf::Vector2f(x,y);
		}

		{
			float const dz = sf::Joystick::getAxisPosition(0, XboxGP::Trig_R)
							-sf::Joystick::getAxisPosition(0, XboxGP::Trig_L);
			recursiveRenderer.zoomIn(dz/2048.f);
		}
	}
}


