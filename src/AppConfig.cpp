#include "AppConfig.hpp"
#include <exception>

AppConfig::AppConfig()
{
	if (font.loadFromFile("/usr/share/fonts/truetype/ubuntu/UbuntuMono-B.ttf")) {}
	else if (font.loadFromFile("/usr/share/fonts/truetype/freefont/FreeMonoBold.ttf")) {}
	else if (font.loadFromFile("/usr/share/fonts/truetype/freefont/FreeSans.ttf")) {}
	else
	{
		throw std::runtime_error("AppConfig could not load font");
	}

	{
		std::string fullHelp =
#include "help.md"
		;
		helpText = fullHelp.substr(fullHelp.find("## Com"));
	}

}


