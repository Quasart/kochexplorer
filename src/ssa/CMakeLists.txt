cmake_minimum_required(VERSION 3.0)
project (SimpleSfmlApp)

find_package(SFML 2 COMPONENTS system window graphics REQUIRED)

add_library(${PROJECT_NAME} INTERFACE)
target_include_directories(${PROJECT_NAME} INTERFACE .)
target_link_libraries(${PROJECT_NAME} INTERFACE sfml-window sfml-graphics)

install(TARGETS ${PROJECT_NAME})

