#pragma once

// Some generic definitions made for SFML-based app.

#include <SFML/Graphics.hpp>
#include <SFML/System/Vector2.hpp>
#include <cmath>
#include <algorithm>


namespace ssa {

template<typename T>
float normTchebychev(sf::Vector2<T> const & a)
{
	return std::max( std::abs(a.x), std::abs(a.y) );
}

template<typename T>
float normManhathan(sf::Vector2<T> const & a)
{
	return std::abs(a.x) + std::abs(a.y);
}

template<typename T>
float normEuclidean(sf::Vector2<T> const & a)
{
	sf::Vector2<T> const a2{a.x*a.x, a.y*a.y};
	return std::sqrt(a2.x + a2.y);
}



template<typename T>
float distanceTchebychev(sf::Vector2<T> const & a, sf::Vector2<T> const & b)
{
	return normTchebychev(a-b);
}

template<typename T>
float distanceManhathan(sf::Vector2<T> const & a, sf::Vector2<T> const & b)
{
	return normManhathan(a-b);
}

template<typename T>
float distanceEuclidean(sf::Vector2<T> const & a, sf::Vector2<T> const & b)
{
	return normEuclidean(a-b);
}

// return rotated vector, trigonometry orientation (x->y)
template<typename T>
sf::Vector2<T> rotate(T alpha, sf::Vector2<T> const & b)
{
	const T c = std::cos(alpha);
	const T s = std::sin(alpha);
	return sf::Vector2<T>(
			c*b.x - s*b.y,
			s*b.x + c*b.y
			);
}

// Linear interpolation
template <typename T>
T lerp(T const & a, T const & b, double t)
{
	return a + t*(b-a);
}

template <typename T>
sf::Vector2<T> lerp(sf::Vector2<T> const & a, sf::Vector2<T> const & b, double t)
{
	return sf::Vector2<T>(
			a.x + t*(b.x-a.x),
			a.y + t*(b.y-a.y)
			);
}

template <typename TX, typename TY>
TY remap( TX const & value, TX const & x1, TY const & y1, TX const & x2, TY const & y2 )
{
	return y1 + (value - x1) * (y2 - y1) / (x2 - x1);
}

// Compute matrix that transforms [O,I] into [A,B]
inline sf::Transform getSegmentTransform(sf::Vector2f a, sf::Vector2f b)
{
	return sf::Transform{ b.x-a.x, a.y-b.y, a.x,
	                      b.y-a.y, b.x-a.x, a.y,
	                      0, 0, 1 };
}

} // ssa
