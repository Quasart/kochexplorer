#pragma once

#include <SFML/Graphics.hpp>
#include <sstream>
#include <cmath>


namespace ssa {



// Rectangle non `sf::Transformable` mais permet d'avoir des coordonnées de texture non entières.
class BasicRectangle : public sf::Drawable
{
public:

	void draw(sf::RenderTarget &target, sf::RenderStates states) const override
	{
		sf::VertexArray vertices(sf::TriangleStrip, 4);

		vertices[0] = sf::Vertex(
				sf::Vector2f{shape.left,shape.top},
				color,
				sf::Vector2f{texCoord.left, texCoord.top} );
		vertices[1] = sf::Vertex(
				sf::Vector2f{shape.left+shape.width,shape.top},
				color,
				sf::Vector2f{texCoord.left+texCoord.width, texCoord.top}
				);
		vertices[2] = sf::Vertex(
				sf::Vector2f{shape.left,shape.top+shape.height},
				color,
				sf::Vector2f{texCoord.left, texCoord.top+texCoord.height} );

		vertices[3] = sf::Vertex(
				sf::Vector2f{shape.left+shape.width,shape.top+shape.height},
				color,
				sf::Vector2f{texCoord.left+texCoord.width, texCoord.top+texCoord.height} );

		if (states.texture == nullptr)
		{
			states.texture = texture;
		}

		target.draw(vertices, states);
	}

	sf::Color color;
	sf::FloatRect shape;
	sf::FloatRect texCoord{0.f, 0.f, 1.f, 1.f};
	sf::Texture const * texture{nullptr};
};


// Optimise la serialisation quand le sf::Text n'est pas affiché
class OptionalText : public sf::Drawable
{
public:
	mutable sf::Text text;
	bool isEnabled{false};
	
	void reset()
	{
		oss.str("");
	}

	void draw(sf::RenderTarget &target, sf::RenderStates states) const override
	{
		if (isEnabled)
		{
			text.setString(oss.str());
			target.draw(text, states);
		}
	}

	template <typename T>
	OptionalText & operator<<(const T & t)
	{
		if (isEnabled)
		{
			oss << t;
		}
		return *this;
	}
private:
	std::stringstream oss;
};

// Efface le text après une certaine periode
class NotifText : public sf::Drawable
{
public:
	void update()
	{
		sf::Time currentTime = clock.getElapsedTime();
		if ( (displayDuration<currentTime) && (displayDuration+fadeDuration > currentTime) )
		{
			unsigned char fade = 0xFF - std::lround( (currentTime-displayDuration).asSeconds() / fadeDuration.asSeconds() * 0xFF );
			text.setFillColor( color * sf::Color{0xFF,0xFF,0xFF,fade} );
		}
	}

	void notify(sf::String const & str)
	{
		text.setString(str);
		text.setFillColor(color);
		clock.restart();
	}

	void draw(sf::RenderTarget &target, sf::RenderStates states) const override
	{
		if (text.getFillColor().a)
		{
			target.draw(text, states);
		}
	}

	sf::Text text;
	sf::Color color;
	sf::Time displayDuration{sf::seconds(15)};
	sf::Time fadeDuration{sf::seconds(2)};
private:
	sf::Clock clock;

};

// Rectangle semi-transparent gerant les transitions fadein/fadeout
class Rideau : public sf::Drawable
{
public:
	Rideau()
	{
		rectangle.shape.top    = -1000000.f;
		rectangle.shape.left   = -1000000.f;
		rectangle.shape.width  = 10000000.f;
		rectangle.shape.height = 10000000.f;
	}

	bool isFull() const { return currentFade == MaxFade; }
	bool isOff() const  { return currentFade == 0; }

	// Démarre un fondu.
	void fadeOut(double time) { fadeSpeed =  std::lround(MaxFade/time); }
	void fadeIn(double time)  { fadeSpeed = -std::lround(MaxFade/time); }

	// Définit un niveau de transparence constant.
	void fadeRatio(double ratio)
	{
		currentFade = std::lround(MaxFade*ratio);
		fadeSpeed = 0;
	}

	void update(double time)
	{
		if (fadeSpeed)
		{
			currentFade += std::lround(fadeSpeed*time);
			if (currentFade>MaxFade)
			{
				currentFade = MaxFade;
				fadeSpeed = 0;
			}
			else if (currentFade<0)
			{
				currentFade = 0.;
				fadeSpeed = 0;
			}
			static constexpr int conversionRatio = (MaxFade+1) >> 8;
			unsigned char screenFade = currentFade / conversionRatio;
			rectangle.color =  color * sf::Color{0xFF,0xFF,0xFF,screenFade};
		}
	}

	void draw(sf::RenderTarget &target, sf::RenderStates states) const override
	{
		if (not isOff())
		{
			target.draw(rectangle, states);
		}
	}

	sf::Color color{sf::Color::Black};
	ssa::BasicRectangle rectangle;
private:
	static constexpr int MaxFade{0xFFFFFFF};
	int currentFade{0}; // [0;MaxFade]
	int fadeSpeed{0};   // fade increment par seconde
};

} // namespace ssa
