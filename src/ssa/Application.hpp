#pragma once
#include <SFML/Graphics.hpp>
#include <chrono>
#include <iomanip>
#include <thread>
#include <deque>
#include "geometry.hpp"
#include "drawable.hpp"

namespace ssa {

enum class EventType { Hovered, Dragged, Droped, Clicked };
struct Event {
	EventType eventType;
	int target;
	sf::Vector2i shift;
};

struct MouseDrag {
	static const int WINDOW = -1;
	struct HoveringZone{
		sf::Vector2i point;
		int target;
	};
	std::vector<HoveringZone> rules;

	std::deque<Event> events;

	int currentTarget = WINDOW;
	int lastTarget = WINDOW;

	void start(sf::Event const & event)
	{
		if (event.type == sf::Event::MouseButtonPressed)
		{
			isDragging = true;
			dragOrigin = sf::Vector2i{ event.mouseButton.x, event.mouseButton.y };
			lastMovePosition = dragOrigin;
		}
	}

	void stop(sf::Event const &event)
	{
		if (isDragging) {
			if (dragShift.x or dragShift.y) {
				events.push_back( Event{EventType::Droped,currentTarget,{event.mouseButton.x,event.mouseButton.y}} );
			}
			else {
				events.push_back( Event{EventType::Clicked,currentTarget,{0,0}} );
			}
			dragShift = {0,0};
			isDragging = false;
		}
	}

	void move(sf::Event const & event)
	{
		if (isDragging)
		{
			const sf::Vector2i dragCurrent{ event.mouseMove.x, event.mouseMove.y };
			dragShift = (dragCurrent - dragOrigin);
			const sf::Vector2i dragMove = dragCurrent - lastMovePosition;
			lastMovePosition = dragCurrent;
			events.push_back( Event{EventType::Dragged,currentTarget,dragMove} );
		}
		else // is Hovering
		{
			currentTarget = WINDOW;
			for (auto const & rule : rules)
			{
				// TODO select min distance
				if (ssa::distanceManhathan(rule.point, sf::Vector2i{ event.mouseMove.x, event.mouseMove.y }) < 20)
				{
					currentTarget = rule.target;
					break;
				}
			}
			if (currentTarget != lastTarget) {
				events.push_back( Event{EventType::Hovered,currentTarget,{0,0}} );
				lastTarget = currentTarget;
			}
		}
	}

	sf::Vector2i dragShift = {0,0};

private:
	bool isDragging = false;
	sf::Vector2i dragOrigin = {0,0};
	sf::Vector2i lastMovePosition = {0,0};
};

struct ProgramOptions {
	bool isFullscreen = false;
	bool isDebug = false;
};


template <typename Config>
class Application
{
public:

	Config const & getConfig() { return config; }

	Application(ProgramOptions const & po)
		: window(po.isFullscreen ? sf::VideoMode::getDesktopMode() : sf::VideoMode(799,600),
				config.appName,
				po.isFullscreen ? sf::Style::Fullscreen : sf::Style::Default,
				sf::ContextSettings(0,0,8) ) // Antialiasing
	{
		debugInfo.isEnabled = po.isDebug;

		//window.setVerticalSyncEnabled(true);
		//window.setKeyRepeatEnabled(false);
		window.setPosition( { 15, 0 } );

	}

private:
	virtual void loadRessources() {}
	virtual void onWindowResize() {}
	virtual void updateState() {}
	virtual void handleEvent(sf::Event const &) {}
	virtual void handleSSAEvent(ssa::Event const &) {}
	virtual void handleKeyPressed() {}
	virtual void drawWindow() {}

	void loadRessources_generic() {
		debugInfo.text.setFillColor(config.colorHighlight);
		debugInfo.text.setFont(config.font);

		helpText.setFillColor(config.colorHighlight);
		helpText.setFont(config.font);
		helpText.setString(config.helpText);

		status.displayDuration = sf::seconds(1);
		status.fadeDuration = sf::seconds(2);
		status.color = config.colorHighlight;
		status.text.setFont(config.font);
	}

	void onWindowResize_generic() {
		const int minSide = std::min(window.getSize().x,window.getSize().y);
		debugInfo.text.setCharacterSize( minSide/50.f);
		debugInfo.text.setPosition(minSide/50.f, minSide/50.f);

		helpText.setCharacterSize( minSide/35 );
		helpText.setPosition(window.getSize().x/7.f, 50);

		status.text.setCharacterSize( minSide/35 );
		status.text.setPosition( 10, window.getSize().y - 30 );
	}

	void handleEvent_generic(sf::Event const & event) {
		if (event.type == sf::Event::KeyPressed)
		{
			if (event.key.code == sf::Keyboard::Q)
			{
				appState = AppState::Quit;
			}
			else if (event.key.code == sf::Keyboard::F1)
			{
				isShowingHelp = !isShowingHelp;
			}
			else if (event.key.code == sf::Keyboard::F8)
			{
				debugInfo.isEnabled = !debugInfo.isEnabled;
			}
			else if (event.key.code == sf::Keyboard::Escape)
			{
				isShowingHelp = false;
				debugInfo.isEnabled = false;
			}
		}
		else if (event.type == sf::Event::Closed)
		{
			appState = AppState::Quit;
		}
		else if (event.type == sf::Event::Resized)
		{
			sf::View windowView( { 0.f, 0.f, (float)event.size.width, (float)event.size.height } );
			window.setView(windowView);
			onWindowResize_generic();
			onWindowResize();
		}
		else if (event.type == sf::Event::MouseButtonPressed)
		{
			//if (event.mouseButton.button == 0)
			mouseDrag.start(event);
		}
		else if (event.type == sf::Event::MouseMoved)
		{
			mouseDrag.move(event);
		}
	}

	void handleKeyPressed_generic() {}

public:
	void run()
	{
		using namespace std::literals::chrono_literals;

		// init time handling

		frameTime = clock.getElapsedTime();


		// init ressources

		loadRessources_generic();
		loadRessources();

		onWindowResize_generic();
		onWindowResize();


		// Window loop
		while (window.isOpen())
		{
			frameDuration = clock.getElapsedTime() - frameTime;
			frameTime = clock.getElapsedTime();
			frameCount++;
			debugInfo.reset();

			// Handle State
			if (appState == AppState::Quit)
			{
				if (rideau.isFull())
				{
					window.close();
				}
				rideau.fadeOut(.2);
				isShowingHelp = false;
			}
			else {
				rideau.fadeRatio(isShowingHelp ? 0.8 : 0.0);
			}

			// Check all the window's events that were triggered since the last iteration of the loop
			sf::Event event;
			while (window.pollEvent(event))
			{
				handleEvent_generic(event);
				handleEvent(event);
				if (event.type == sf::Event::MouseButtonReleased)
				{
					//if (event.mouseButton.button == 0)
					mouseDrag.stop(event);
				}
			}

			while (not mouseDrag.events.empty())
			{
				handleSSAEvent(mouseDrag.events.front());
				mouseDrag.events.pop_front();
			}

			// Update state (non-event based)

			if (window.hasFocus()) {
				handleKeyPressed_generic();
				handleKeyPressed();
			}

			{
				int fps = 1000./frameDuration.asMilliseconds();
				debugInfo << std::setprecision(3) <<
					"Fps: "      << fps << " (" << frameDuration.asMilliseconds() << " ms = "
					<< processingDuration.asMilliseconds() << " ms proc + " << renderingDuration.asMilliseconds() << " ms render)\n";
			}

			rideau.update(frameDuration.asSeconds());
			status.update();
			updateState();

			processingDuration = clock.getElapsedTime() - frameTime;


			// Draw

			window.clear(config.colorBackground);
			drawWindow();

			window.draw(rideau);
			window.draw(debugInfo);
			window.draw(status);

			if (isShowingHelp)
			{
				window.draw(helpText);
			}

			window.display();

			renderingDuration = clock.getElapsedTime() - frameTime - processingDuration;


			// Sleep if possible to lower cpu consumption.
			if (!window.hasFocus())
			{
				std::this_thread::sleep_for(500ms);
			}
			else if (frameDuration.asMilliseconds() < 50)
			{
				std::this_thread::sleep_for(10ms);
			}
		}
	}

private:
	Config config;

	enum class AppState {
		Normal,
		Quit
	};
	AppState appState = AppState::Normal;

	sf::Clock clock;

protected:
	sf::Time frameTime;
	sf::Time frameDuration;
	sf::Time processingDuration;
	sf::Time renderingDuration;

	sf::RenderWindow window;
	sf::Text helpText;
	OptionalText debugInfo;
	Rideau rideau;
	NotifText status;

	MouseDrag mouseDrag;

	size_t frameCount = 0;
	bool isShowingHelp = false;

};

} // ssa
