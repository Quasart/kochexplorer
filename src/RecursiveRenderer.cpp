#include "RecursiveRenderer.hpp"


void RecursiveRenderer::processQueue(sf::RenderTarget & window, RecursiveRule const & rule)
{
	// screen center
	sf::Vector2f const O{ window.getSize().x/2.f,window.getSize().y/2.f };

	nbIteration = 0;
	segmentTotalLength = 0;

	while (!queue.empty())
	{
		nbIteration++;

		Step step = std::move(queue.front());
		queue.pop_front();

		auto A = step.transform.transformPoint(rule.recursiveLine.begin()->position);
		auto B = step.transform.transformPoint(rule.recursiveLine.rbegin()->position);

		// [A,B] midpoint
		sf::Vector2f const M = (A+B)/2.f;

		float const distAB = ssa::distanceTchebychev(A,B);
		float const distOM = ssa::distanceTchebychev(M,O);
		float const distOA = ssa::distanceTchebychev(A,O);
		float const distOB = ssa::distanceTchebychev(B,O);
		float const distScreen = std::min(std::min( distOA, distOB), distOM ) / window.getSize().x;

		int shade = std::lround(ssa::remap<float>(step.depth,getMaxNbOfRecursions(),0xff,0,0));
		const sf::Color c(shade, 0xff, 0xff);

		if ((nbIteration+queue.size() > 0x40000) or
			(segmentTotalLength+queue.size()*detailThreshold > 0x8000))
		{
			// drawing too much => stop
			if (step.depth > 0) step.depth = 0;
		}

		if ( distAB < 0.001 )
		{
			// means A==B => skip
		}
		else if ( ( distOM > distAB ) and ( distScreen > .5 ) )
		{
			// means off screen => skip
		}
		else if ( (!step.depth) or (distAB<detailThreshold) )
		{
			// break recursion
			segmentsToDraw.emplace_back(A,c);
			segmentsToDraw.emplace_back(B,c);
			segmentTotalLength += std::lround(distAB);
		}
		else
		{
			size_t const nbSegments = rule.recursiveLine.size()-1;
			for (size_t i=0; i<nbSegments; ++i)
			{
				if (rule.recursiveLine.getLegType(i) == LegType::Terminal)
				{
					segmentsToDraw.emplace_back(
							step.transform.transformPoint(rule.recursiveLine[i].position),
							c);
					segmentsToDraw.emplace_back(
							step.transform.transformPoint(rule.recursiveLine[i+1].position),
							c);
				}
				else
				{
					Step newStep;
					newStep.depth = step.depth-1;
					newStep.transform = step.transform*getSegmentTransform(rule.recursiveLine, i);

					if (rule.shouldAlternateX)
					{
						static const sf::Transform invX{
							-1, 0, 1,
							0, 1, 0,
							0, 0, 1 };
						newStep.transform *= invX;
					}

					if (rule.shouldAlternateY)
					{
						static const sf::Transform invY{
							1,  0, 0,
							0, -1, 0,
							0,  0, 1 };
						newStep.transform *= invY;
					}

					// LIFO pour les gros segments, FIFO pour les details
					if (distAB<32.)
						queue.push_back(newStep);
					else
						queue.push_front(newStep);
				}
			}
		}
	}
}

void RecursiveRenderer::computeShape(sf::RenderTarget & window, RecursiveRule const & rule, Line const & shape, sf::Transform const & initialTransform)
{
	queue.clear();
	segmentsToDraw.clear();

	for (size_t i=0; i<shape.size()-1; i++)
	{
		if (shape.getLegType(i) == LegType::Terminal)
		{
			segmentsToDraw.emplace_back(
					initialTransform.transformPoint(shape[i].position),
					sf::Color::White);
			segmentsToDraw.emplace_back(
					initialTransform.transformPoint(shape[i+1].position),
					sf::Color::White);
		}
		else
		{
			queue.emplace_back();
			queue.back().depth = getMaxNbOfRecursions();
			queue.back().transform = initialTransform*getSegmentTransform(shape,i);
		}
	}

	processQueue(window,rule);
}

void RecursiveRenderer::compute(sf::RenderTarget & window, RecursiveRule const & rule, sf::Transform const & initialTransform)
{
	queue.clear();
	segmentsToDraw.clear();

	queue.emplace_back();
	queue.back().depth = getMaxNbOfRecursions();
	queue.back().transform = initialTransform;

	processQueue(window,rule);
}

void RecursiveRenderer::draw(sf::RenderTarget & window)
{
	window.draw(segmentsToDraw.data(), segmentsToDraw.size(), sf::Lines);
}
