#pragma once

#include <SFML/Graphics.hpp>
#include "Shape.hpp"
#include <deque>


// Rendering parameters : view and rendering quality

struct RecursiveRenderer
{
	static constexpr float defaultZoomFactor = 1000.f;
	float zoomFactorCmd = defaultZoomFactor;          // Raw command
	float zoomFactor    = defaultZoomFactor/100.f;    // Applied command (after smoothing)

	sf::Vector2f viewShift{};
	float detailThreshold = 1.;
	bool forceLowDef = false;
	size_t maxNbRecursions = 125;
	size_t maxNbRecursionsLD = 5;
	bool shouldDrawFullShape = true;

	struct Step {
		size_t depth;
		sf::Transform transform;
	};
	std::deque<Step> queue;

	std::vector<sf::Vertex> segmentsToDraw;
	size_t segmentTotalLength = 0; // To count number of segment displayed per frame
	size_t nbIteration = 0;

	size_t & getMaxNbOfRecursions()
	{
		return (forceLowDef?maxNbRecursionsLD:maxNbRecursions);
	}
	void recLevelIncrease()
	{
		auto & maxNb = getMaxNbOfRecursions();
		++maxNb;
	}
	void recLevelDecrease()
	{
		auto & maxNb = getMaxNbOfRecursions();
		if (maxNb > 0)
		{
			--maxNb;
		}
	}
	void increaseDetailThreshold()
	{
		detailThreshold *= 2;
	}

	void decreaseDetailThreshold()
	{
		detailThreshold *= 0.5;
	}

	void switchHighLowDef()
	{
		forceLowDef = !forceLowDef;
	}

	// delta vector in view scale (move after zoom application)
	void moveView(sf::Vector2f delta)
	{
		viewShift -= delta/zoomFactor;
	}

	void zoomIn(float delta)
	{
		zoomFactorCmd *= (1.f+delta);
	}
	void resetView()
	{
		zoomFactorCmd = defaultZoomFactor;
		viewShift = {};
	}

	void update(sf::Time frameDuration)
	{
		float const step = frameDuration.asSeconds();

		// Avoid ZoomFactor jump (needed for mousewheel input)
		zoomFactor += (zoomFactorCmd - zoomFactor) * std::min(1., step*4.);
	}

	void processQueue(sf::RenderTarget & window, RecursiveRule const & rule);
	void compute(sf::RenderTarget & window, RecursiveRule const & rule, sf::Transform const & initialTransform);
	void computeShape(sf::RenderTarget & window, RecursiveRule const & rule, Line const & shape, sf::Transform const & initialTransform);
	size_t getSegmentCount() const { return segmentsToDraw.size() / 2; }
	void draw(sf::RenderTarget & window);
};


