#include <iostream>
#include "KochExplorer.hpp"

using namespace std::literals::string_literals;

int main(int argc, const char ** argv)
{
	ssa::ProgramOptions po;

	for (int i=0; i<argc; i++)
	{
		std::string const arg = argv[i];
		if (arg == "--help" || arg == "-h") {
			std::cout << "options:" << std::endl;
			std::cout << " -d, --debug" << std::endl;
			std::cout << " -f, --fullscreen" << std::endl;
			std::cout << " -h, --help" << std::endl;
			std::cout << " -v, --version" << std::endl;
			return 0;
		}
		else if (arg == "--version" || arg == "-v") {
			std::cout << "Version " << AppConfig::appVersion << " - Build " << __DATE__ << std::endl;
			return 0;
		}
		else if (arg == "--fullscreen" || arg == "-f") { po.isFullscreen = true; }
		else if (arg == "--debug" || arg == "-d") { po.isDebug = true; }
	}

	KochExplorer app(po);
	app.run();

	return 0;
}

