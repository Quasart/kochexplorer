#pragma once

#include <SFML/Graphics.hpp>

enum class LegType
{
	SelfSimilar = 0b000,
	InvertX     = 0b001,
	InvertY     = 0b010,
	InvertXY    = 0b011,
	Terminal    = 0b100,
};

class Line : public std::vector<sf::Vertex>
{
public:
	using Super = std::vector<sf::Vertex>;
	using Super::Super;

	static LegType getLegType(sf::Vertex const & v)
	{
		return static_cast<LegType>(v.texCoords.x);
	}

	LegType getLegType(size_t legId) const
	{
		return getLegType(at(legId+1));
	}

	static void setLegType(sf::Vertex & v, LegType t)
	{
		v.texCoords.x = float( static_cast<char>(t) );
	}

	void setLegType(size_t legId, LegType t)
	{
		setLegType(at(legId+1),t);
	}
	
	static const char * getLegType_str( LegType t )
	{
		static const char * names[] = {
			"SelfSimilar",
			"InvertX",
			"InvertY",
			"InvertXY",
			"Terminal"
		};
		return names[ static_cast<size_t>(t) ];
	}


	bool doesLegInvertX(size_t i) const
	{
		return static_cast<int>(getLegType(i)) & static_cast<int>(LegType::InvertX);
	}

	bool doesLegInvertY(size_t i) const
	{
		return static_cast<int>(getLegType(i)) & static_cast<int>(LegType::InvertY);
	}

	void toggleLegInvertX(size_t i)
	{
		auto t = getLegType(i);
		if (t != LegType::Terminal)
		{
			auto bits = static_cast<int>(t);
			bits ^= static_cast<int>(LegType::InvertX);
			setLegType(i,static_cast<LegType>(bits));
		}
	}

	void toggleLegInvertY(size_t i)
	{
		auto t = getLegType(i);
		if (t != LegType::Terminal)
		{
			auto bits = static_cast<int>(t);
			bits ^= static_cast<int>(LegType::InvertY);
			setLegType(i,static_cast<LegType>(bits));
		}
	}

	void toggleLegTerminal(size_t i)
	{
		auto t = getLegType(i);
		setLegType(i,(t==LegType::Terminal) ? LegType::SelfSimilar : LegType::Terminal);
	}


	static const Line quadraticLine;
	static const Line kochLine;
	static const Line noShape;
	static const Line triangleShape;
	static const Line squareShape;

	static const sf::Vector2f kSelfSimilar;
	static const sf::Vector2f kTerminal;
	static const sf::Vector2f kInvertX;
	static const sf::Vector2f kInvertY;
	static const sf::Vector2f kInvertXY;
};

#include <cassert>
#include "ssa/geometry.hpp"
inline sf::Transform getSegmentTransform(Line const & line, size_t i)
{
	assert( line.size()>1 );
	assert( (i+1) < line.size() );

	auto const & a = line[i].position;
	auto const & b = line[i+1].position;

	sf::Transform t = ssa::getSegmentTransform(a,b);
	if (line.doesLegInvertX(i))
	{
		static const sf::Transform invX{
			-1, 0, 1,
			0, 1, 0,
			0, 0, 1 };
		t *= invX;
	}

	if (line.doesLegInvertY(i))
	{
		static const sf::Transform invY{
			1,  0, 0,
			0, -1, 0,
			0,  0, 1 };
		t *= invY;
	}
	return t;
}


struct RecursiveRule
{
	Line recursiveLine;
	bool shouldAlternateX{false};
	bool shouldAlternateY{false}; // mode altérné inspiré par Mickaël Launay: www.youtube.com/watch?v=8D_ThIqoJL8
};


// Controller that can change Line vertex positions

class ShapeController
{
private:
	size_t currentIndex{0};
	Line * currentLine{nullptr};

public:

	void setCurrentLine(Line & l)
	{
		currentLine = &l;
		currentIndex = std::min( currentIndex, l.size()-1 );
	}

	Line const & getCurrentLine() const
	{
		return *currentLine;
	}

	void selectNext()
	{
		if (currentIndex < (currentLine->size()-1))
			++currentIndex;
	}

	void selectBack()
	{
		if (currentIndex > 0)
			--currentIndex;
	}

	sf::Vertex & getCurrentVextex()
	{
		return currentLine->at(currentIndex);
	}

	void setCurrentVertex(size_t i)
	{
		currentIndex = i;
		if (currentIndex >= currentLine->size())
		{
			currentIndex = currentLine->size()-1;
		}
	}

	void toggleInvertX()
	{
		if (currentIndex>0)
			currentLine->toggleLegInvertX(currentIndex-1);
	}

	void toggleInvertY()
	{
		if (currentIndex>0)
			currentLine->toggleLegInvertY(currentIndex-1);
	}

	void toggleTerminal()
	{
		if (currentIndex>0)
			currentLine->toggleLegTerminal(currentIndex-1);
	}


	// vector control
	
	void deleteCurrent()
	{
		if (not currentLine->empty() and currentIndex)
		{
			currentLine->erase( currentLine->begin()+currentIndex );
			--currentIndex;
		}
	}

	void append()
	{
		currentLine->emplace_back( sf::Vector2f(1.f,0.f) );
		currentIndex = currentLine->size()-1;
	}

	void divideCurrentLeg()
	{
		if (currentIndex>0)
		{
			sf::Vertex a = currentLine->at(currentIndex-1);
			sf::Vertex b = currentLine->at(currentIndex);
			sf::Vertex v = b;
			v.position.x = (a.position.x + b.position.x) / 2.f;
			v.position.y = (a.position.y + b.position.y) / 2.f;
			currentLine->insert( currentLine->begin()+currentIndex, v );
		}
	}

};

