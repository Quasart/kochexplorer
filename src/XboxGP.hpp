#pragma once

#include <SFML/Window/Joystick.hpp>

// utils for XBox Gamepad handling with SFML

namespace XboxGP
{
	enum BtnEnum
	{
		Btn_A = 0,
		Btn_B,
		Btn_X,
		Btn_Y,
		Btn_L,
		Btn_R,
		Btn_Back,
		Btn_Start,
		Btn_Logo,
		Btn_LJ,
		Btn_RJ,
		Btn_Left,
		Btn_Right,
		Btn_Up,
		Btn_Down
	};

	const sf::Joystick::Axis Joystick_LX = sf::Joystick::X;
	const sf::Joystick::Axis Joystick_LY = sf::Joystick::Y;
	const sf::Joystick::Axis Joystick_RX = sf::Joystick::U;
	const sf::Joystick::Axis Joystick_RY = sf::Joystick::V;
	const sf::Joystick::Axis Trig_L = sf::Joystick::Z;
	const sf::Joystick::Axis Trig_R = sf::Joystick::R;

	// To snap joystick position to zero when under threshold.
	inline float joystickCurved(float x)
	{
		static const float threshold = 24.f;
		if (x>threshold) { return (x-threshold)*(x-threshold)/128.f; }
		else if (x<-threshold) { return -(x+threshold)*(x+threshold)/128.f; }
		else { return 0.f; }
	}

}
