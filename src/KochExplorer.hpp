#pragma once
#include "Shape.hpp"
#include "RecursiveRenderer.hpp"
#include "AppConfig.hpp"
#include "ssa/Application.hpp"


class KochExplorer : public ssa::Application<AppConfig>
{
public:
	KochExplorer(ssa::ProgramOptions const & po);

private:
	void onWindowResize() override;
	void updateState() override;
	void handleEvent(sf::Event const & event) override;
	void handleSSAEvent(ssa::Event const & event) override;
	void handleKeyPressed() override;
	void drawWindow() override;

	void writeParams(std::string const & filename);
	void save();
	void drawMinimap(sf::RenderWindow & window, Line const & line, sf::Time time, sf::Transform const & initialTransform);

	float getSpeedFactor() const;

private:
	sf::Transform miniMapZoom;

	Line baseShape;
	RecursiveRule recRule;
	ShapeController shapeCtrl;
	RecursiveRenderer recursiveRenderer;
	bool shouldDrawMinimap{true};
};
