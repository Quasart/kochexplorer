#pragma once

#include <SFML/Graphics.hpp>
#include <string>

class AppConfig {
public:

	AppConfig();

	static constexpr const char * appName = "KochExplorer";
	static constexpr const char * appVersion = "0.2";
	std::string helpText;

	// style
	sf::Font font;

	const sf::Color colorForeground{sf::Color::White};
	const sf::Color colorHighlight{0x00,0xAA,0xCC};
	const sf::Color colorBackground{sf::Color::Black};

};

