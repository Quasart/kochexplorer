#include "Shape.hpp"

const Line Line::quadraticLine{
	sf::Vertex( {0.f,    0.f  } ),
	sf::Vertex( {0.25f,  0.f  } ),
	sf::Vertex( {0.25f,  0.25f} ),
	sf::Vertex( {0.50f,  0.25f} ),
	sf::Vertex( {0.50f,  0.f  } ),
	sf::Vertex( {0.50f, -0.25f} ),
	sf::Vertex( {0.75f, -0.25f} ),
	sf::Vertex( {0.75f, -0.f  } ),
	sf::Vertex( {1.f,    0.f  } )
};
const Line Line::kochLine{
	sf::Vertex( {0.f,       0.f  } ),
	sf::Vertex( {.333333f,  0.f  } ),
	sf::Vertex( {.5f,      -0.28f} ),
	sf::Vertex( {.666667f,  0.f  } ),
	sf::Vertex( {1.f,       0.f  } )
};



const Line Line::noShape{
	sf::Vertex( sf::Vector2f{0.0f, 0.0f} ),
	sf::Vertex( sf::Vector2f{1.0f, 0.0f} )
};
const Line Line::triangleShape{
	sf::Vertex( sf::Vector2f{0.0f, 0.0f}   ),
	sf::Vertex( sf::Vector2f{1.0f, 0.0f}   ),
	sf::Vertex( sf::Vector2f{0.5f, 0.866f} ),
	sf::Vertex( sf::Vector2f{0.0f, 0.0f}   )
};
const Line Line::squareShape{
	sf::Vertex( sf::Vector2f{0.0f, 0.0f} ),
	sf::Vertex( sf::Vector2f{1.0f, 0.0f} ),
	sf::Vertex( sf::Vector2f{1.0f, 1.0f} ),
	sf::Vertex( sf::Vector2f{0.0f, 1.0f} ),
	sf::Vertex( sf::Vector2f{0.0f, 0.0f} )
};
