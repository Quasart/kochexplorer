R"(
## Program options

* `--debug` or `-d` to display debug statistics on screen.
* `--fullscreen` or `-f` to start fullscreen.
* `--help` or `-h` to print this help information.
* `--version` or `-v` to print version.

## Commands

| Action                                    | Keyboard/Mouse     | XBox Gamepad |
|-------------------------------------------|:------------------:|:------------:|
| Move view                                 | Mouse drag         | L Joystick   |
| Zoom in/Zoom out                          | Mouse wheel        | L/R Trig     |
| Reset view                                | Home               |              |
|                                           |                    |              |
| Select next vertex/segment                | Tab                | Right        |
| Select previous vertex/segment            | Shift Tab          | Left         |
| Delete current vertex/segment             | Delete / Backspace | B            |
| Move current vertex                       | Arrow keys         | R Joystick   |
| Append vertex (at the end)                | A                  |              |
| Divide current segment, add middle vertex | D                  | A            |
| Toogle current segment rec termination    | T                  | Right Button |
| Toogle current segment rec orientation    | X / Y              | X / Y        |
| Toggle Alernate rec orientation           | Shift X / Shift Y  |              |
|                                           |                    |              |
| Increase max recursion depth              | Page Down          |              |
| Decrease max recursion depth              | Page Up            |              |
| Increase rendering precision              | Shift Page Down    |              |
| Decrease rendering precision              | Shift Page Up      |              |
| Toggle Low-def mode                       | W                  |              |
| Toggle Full-Shape render                  | V                  |              |
| Toggle Base shape edition                 | L                  | Up / Down    |
|                                           |                    |              |
| Faster...                                 | Shift              |              |
| Slower...                                 | Alt                |              |
| Quit                                      | Q                  |              |
| Toggle minimap display                    | M                  |              |
| Toggle help screen display                | F1                 |              |
| Toggle debug info display                 | F8                 |              |
| Save to file                              | Ctrl S             |              |

)"
