FROM debian
RUN apt-get update && apt-get install --no-install-recommends -y make cmake g++ libsfml-dev
VOLUME /project
VOLUME /build
WORKDIR /build
CMD cmake /project && make
