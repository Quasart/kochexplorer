all:
	mkdir -p build
	cd build && cmake .. && make

docker:
	docker run --read-only -v `pwd`:/project -v `pwd`/build:/build kockexplorer_buildenv:v1
